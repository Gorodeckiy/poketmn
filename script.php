<?php

//date_default_timezone_set("Asia/Yekaterinburg");
date_default_timezone_set("UTC");

try {
	$dbh = new PDO('sqlite:/home/poketmn/PokemonGo-Map-develop/pogom.db');
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //Error Handling
	$dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	
	foreach ($dbh->query("SELECT * FROM pokemon") as $row) {
		if ($spawns["$row[spawnpoint_id]"]) {
			if ($spawns["$row[spawnpoint_id]"][lat] != $row[latitude] OR $spawns["$row[spawnpoint_id]"]['long'] != $row[longitude])
				echo "{$spawns[$row[spawnpoint_id]][lat]}!=$row[latitude] OR {$spawns[$row[spawnpoint_id]]['long']}!=$row[longitude] \n";
		} else
			$spawns["$row[spawnpoint_id]"] = array(
				'lat' => (float) $row[latitude],
				'long' => (float) $row[longitude],
				'since' => time()
			);
		$row[pokemon_id]                                                 = (string) $row[pokemon_id];
		$row[disappear_time]                                             = strtotime($row[disappear_time]);
		//$spawns[$row[spawnpoint_id]][pokemons][$row[pokemon_id]][data][]=array('disappear_time'=>$row[disappear_time], 'timestamp'=>strtotime($row[timestamp]));
		$spawns[$row[spawnpoint_id]][pokemons][$row[pokemon_id]][data][] = $row[disappear_time];
		$spawns[$row[spawnpoint_id]][pokemons][$row[pokemon_id]][last]   = max($spawns[$row[spawnpoint_id]][pokemons][$row[pokemon_id]][last], $row[disappear_time]);
		$spawns[$row[spawnpoint_id]][since]                              = min($spawns[$row[spawnpoint_id]][since], $row[disappear_time]);
		$poketotal++;
		$pokecount[$row[pokemon_id]]++;
	}
	foreach ($dbh->query("SELECT * FROM gym") as $row)
		$gyms[] = array(
			(float) $row[longitude],
			(float) $row[latitude]
		);
	foreach ($dbh->query("SELECT * FROM pokestop") as $row)
		$pokestops[] = array(
			(float) $row[longitude],
			(float) $row[latitude]
		);
	
	$dbh = null;
	
	echo date("Y-m-d H:i:s") . " spots: " . count($spawns) . ", pokemons: $poketotal\n";
	
	$pokemons = json_decode(file_get_contents('pokemon.en.json'), 1);
	//asort($pokemons);
	//echo json_encode(array_keys($pokemons));
	
	foreach ($gyms as $val) {
		$feature    = array(
			'type' => 'Feature',
			"geometry" => array(
				"type" => "Point",
				"coordinates" => $val
			),
			"properties" => array(
				"name" => "Gym"
			)
		);
		$features[] = $feature;
	}
	
	foreach ($pokestops as $val) {
		$feature    = array(
			'type' => 'Feature',
			"geometry" => array(
				"type" => "Point",
				"coordinates" => $val
			),
			"properties" => array(
				"name" => "Pokestop"
			)
		);
		$features[] = $feature;
	}
	
	function so($a, $b) {
		return (count($a[data]) > count($b[data]) ? -1 : 1);
	}
	$timemod = strtotime('2016-07-01 0:00:00'); // 1467320400?
	
	foreach ($spawns as $val) {
		
		uasort($val[pokemons], 'so');
		$top      = $text = '';
		$topcount = $count = 0;
		$counts   = array();
		$total    = 0;
		foreach ($val[pokemons] as $pid => $arr) {
			$count = count($arr[data]);
			$total += $count;
			$counts[] = $count;
			if (!$top) {
				$topcount = $count;
				$top      = ($topcount > 2 ? $pid : 'no');
			} elseif ($top != 'no' && $count == $topcount) {
				$top = 'no';
			}
			//if ($text) $text.="<br>";
			//$text.="<img src='/static/pokemon/$pid.png'><b>$pokemons[$pid]</b> seen ".$count." times";
		}
		
		$feature    = array(
			'type' => 'Feature',
			"geometry" => array(
				"type" => "Point",
				"coordinates" => array(
					$val['long'],
					$val[lat]
				)
			),
			"properties" => array(
				"name" => "Spot",
				"top" => $top,
				"pokemons" => array_keys($val[pokemons]),
				"counts" => $counts,
				"since" => $val[since],
				"total" => $total
				//"icon"=> "pokemon/$top.png",
				//"description"=> "$text"
			)
		);
		$features[] = $feature;
	}
	
	file_put_contents('stats.js', "var pokecount = " . json_encode($pokecount, JSON_PRETTY_PRINT) . ";
var poketotal=$poketotal;
var spotstotal=" . count($spawns) . ";
var stopstotal=" . count($pokestops) . ";
var gymstotal=" . count($gyms) . ";
");
	//file_put_contents('map.json','{"type": "FeatureCollection","features": '.json_encode($features,JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES).'}');
	file_put_contents('map.json', '{"type": "FeatureCollection","features": ' . json_encode($features, JSON_UNESCAPED_SLASHES) . '}');
	
}
catch (PDOException $e) {
	print "Error!: " . $e->getMessage() . "<br/>";
	die();
}

?>